/**
 * Created by pl26565 on 6/2/17.
 */
package com.poc.demo.zuul.configuration;

import com.poc.demo.zuul.filter.SimpleFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GatewayBeanConfiguration{

    @Bean
    public SimpleFilter simpleFilter(){
        return new SimpleFilter();
    }
}