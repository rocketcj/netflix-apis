package com.poc.demo.client.feign.anotations;

import org.springframework.stereotype.Component;

/**
 * Created by pl26565 on 5/26/17.
 */
public @interface IgnoreDuringScan {
}
