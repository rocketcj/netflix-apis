package com.poc.demo.client.feign.config;

import com.poc.demo.client.feign.anotations.IgnoreDuringScan;
import feign.Contract;
import feign.auth.BasicAuthRequestInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/*
Created by pl26565 on 5/24/17.
*/

@Configuration
@IgnoreDuringScan
public class FeignClientConfiguration {
    @Bean
    public Contract feignContract() {
        return new feign.Contract.Default();
    }

    @Bean
    public BasicAuthRequestInterceptor basicAuthRequestInterceptor() {
        return new BasicAuthRequestInterceptor("admin", "admin123");
    }
}
