package com.poc.demo.client.feign.client;

import com.poc.demo.client.feign.controller.AccountServiceClientFallBackController;
import com.poc.demo.dataobjects.domain.AccountSummary;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

    /**
     * Created by pl26565 on 5/24/17.
     */
    @FeignClient(value="account-service", fallback = AccountServiceClientFallBackController.class)
    public interface AccountServiceClient {

        @RequestMapping(method = RequestMethod.GET, value = "/accounts")
        List<AccountSummary> retrieveAllAccounts();

        @RequestMapping(method = RequestMethod.GET, value = "/accounts/{acctNumber}")
        AccountSummary getAccountByNumber(@PathVariable("acctNumber") Long accountNumber);

   /* @RequestMapping(method = RequestMethod.POST, value = "/users/{id}", consumes = "application/json")
    User update(@PathVariable("id") Long id, User user);*/
    }

