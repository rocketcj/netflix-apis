package com.poc.demo.client.feign.config;

import com.poc.demo.dataobjects.domain.AccountSummary;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by pl26565 on 5/31/17.
 */


@Configuration
public class AcctBeansConfiguration {

    @Bean
    public AccountSummary accountSummary(){
        return new AccountSummary();
    }

}
