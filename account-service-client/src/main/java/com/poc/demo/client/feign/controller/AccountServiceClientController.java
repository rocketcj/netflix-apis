package com.poc.demo.client.feign.controller;

import com.poc.demo.client.feign.client.AccountServiceClient;
import com.poc.demo.dataobjects.domain.AccountSummary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by pl26565 on 5/30/17.
 */
@Component
@RestController
public class AccountServiceClientController {

    @Autowired
    private AccountServiceClient accountServiceClient;

    @Autowired
    private AccountSummary accountSummary;

    @RequestMapping(method = RequestMethod.GET, value="/accounts")
    public List<AccountSummary> getAccounts() {
        List<AccountSummary> accountSummaryList = accountServiceClient.retrieveAllAccounts();
        return accountSummaryList;
    }

    @Autowired
    public void setAccountServiceClient(AccountServiceClient accountServiceClient){
        this.accountServiceClient = accountServiceClient;

    }
}
