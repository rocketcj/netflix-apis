package com.poc.demo.client.feign;

import com.poc.demo.client.feign.anotations.IgnoreDuringScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by pl26565 on 5/23/17.
 */

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages = "com.poc.demo.client.feign", excludeFilters = @ComponentScan.Filter(IgnoreDuringScan.class))
//@ComponentScan(basePackages = "com.pnc.accountlink.client.feign")
@EnableDiscoveryClient
@EnableFeignClients(basePackages = "com.poc.demo.client.feign.client")
@EnableCircuitBreaker
@EnableHystrix
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}