package com.poc.demo.client.feign.controller;

import com.poc.demo.client.feign.client.AccountServiceClient;
import com.poc.demo.dataobjects.domain.AccountSummary;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pl26565 on 5/25/17.
 */

//@RestController
@Component
public class AccountServiceClientFallBackController implements AccountServiceClient{

    @Override
    public List<AccountSummary> retrieveAllAccounts() {
        List<AccountSummary> accountSummaryList = new ArrayList<AccountSummary>();
        AccountSummary acctSummary = new AccountSummary();
        acctSummary.setId(1L);
        acctSummary.setName("CeeJay-FallBack");
        acctSummary.setAccountType("CHECKING-FallBack");
        acctSummary.setAccountNumber(1234567890123456L);
        accountSummaryList.add(acctSummary);
        return accountSummaryList;

    }

    @Override
    public AccountSummary getAccountByNumber(@PathVariable("acctNumber") Long accountNumber) {
        return null;
    }
}
