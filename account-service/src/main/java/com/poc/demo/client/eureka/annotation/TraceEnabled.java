package com.poc.demo.client.eureka.annotation;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Classes annotated with {@link com.pnc.accountlink.accounts.util.annotation.TraceEnabled}, will not be loaded to the context
 * unless "traceEnabled" profile is activated.
 *
 * @author Chandrasekar Jayaraj
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Configuration
@Profile("trace-enabled")
public @interface TraceEnabled {
}
