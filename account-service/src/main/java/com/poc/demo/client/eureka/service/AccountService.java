package com.poc.demo.client.eureka.service;



import com.poc.demo.dataobjects.domain.AccountSummary;

import java.util.List;

/**
 * Created by pl26565 on 5/24/17.
 */
public interface AccountService {
    List<AccountSummary> getAllAccounts();
    AccountSummary getAccountsByAccountNumber(Long accountNumber);

}
