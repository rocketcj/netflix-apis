package com.poc.demo.client.eureka.annotation;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation for <code>development</code> Environment Configuration.
 * Environment properties are located in <code>application.yml</code>.
 *
 * @author Chandrasekar Jayaraj
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Configuration
@Profile("dev")
public @interface Dev {

}
