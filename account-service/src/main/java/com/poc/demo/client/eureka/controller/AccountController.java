package com.poc.demo.client.eureka.controller;

import com.poc.demo.client.eureka.service.AccountService;
import com.poc.demo.dataobjects.domain.AccountSummary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by pl26565 on 5/24/17.
 */
@RestController
public class AccountController {

    @Autowired
    private final AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService){
        this.accountService = accountService;
    }

    @RequestMapping("/accounts")
    public List<AccountSummary> retrieveAllAccounts() {
        List<AccountSummary> acctSummaryList = accountService.getAllAccounts();
        return acctSummaryList;
    }

    @RequestMapping("/accounts/{acctNumber}")
    public AccountSummary getAccountByNumber(@PathVariable ("acctNumber") Long accountNumber) {
        return accountService.getAccountsByAccountNumber(accountNumber);
    }
}
