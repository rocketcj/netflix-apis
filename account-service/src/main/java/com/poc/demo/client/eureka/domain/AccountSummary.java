package com.poc.demo.client.eureka.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by pl26565 on 5/24/17.
 */

@Entity
@Table(name = "ACCOUNT_SUMMARY")
public class AccountSummary implements Serializable{
    @Id
    @NotNull
    @Column(name = "ID", unique = true, nullable = false)
    private Long id;

    @Column(name="NAME")
    private String name;
    @Column(name="ACCOUNT_TYPE")
    private String accountType;
    @Column(name="ACCOUNT_NUMBER")
    private Long accountNumber;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAccountType() {
        return accountType;
    }

    public Long getAccountNumber() {
        return accountNumber;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public void setAccountNumber(Long accountNumber) {
        this.accountNumber = accountNumber;
    }
}
