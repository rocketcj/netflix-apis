package com.poc.demo.client.eureka.service;

import com.poc.demo.client.eureka.dao.AccountSummaryRepository;
import com.poc.demo.dataobjects.domain.AccountSummary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pl26565 on 5/24/17.
 */
@Service
public class AccountServiceImpl implements AccountService {

    private AccountSummaryRepository accountSummaryRepository;

    @Autowired
    public AccountServiceImpl(AccountSummaryRepository accountSummaryRepository){
        this.accountSummaryRepository = accountSummaryRepository;
    }

    @Override
    public List<AccountSummary> getAllAccounts() {
        return copyListOfAccountSummaryDAOToAccountSummaryDTO(accountSummaryRepository.findAll());
        //return accountSummaryRepository.findAll();
    }

    @Override
        public AccountSummary getAccountsByAccountNumber(Long accountNumber) {
        return copyAccountSummaryDAOToAccountSummaryDTO(accountSummaryRepository.findByAccountNumber(accountNumber));
        //return accountSummaryRepository.findByAccountNumber(accountNumber);
    }

    private List<AccountSummary> copyListOfAccountSummaryDAOToAccountSummaryDTO(List<com.poc.demo.client.eureka.domain.AccountSummary> accountSummaryDAOList){

        List<AccountSummary> acctSummaryDTOList = new ArrayList<AccountSummary>();
        AccountSummary acctSummaryDTO = new AccountSummary();
        for(com.poc.demo.client.eureka.domain.AccountSummary acctSummaryDAO: accountSummaryDAOList){
            acctSummaryDTO.setId(acctSummaryDAO.getId());
            acctSummaryDTO.setName(acctSummaryDAO.getName());
            acctSummaryDTO.setAccountNumber(acctSummaryDAO.getAccountNumber());
            acctSummaryDTO.setAccountType(acctSummaryDAO.getAccountType());
            acctSummaryDTOList.add(acctSummaryDTO);
        }

        return acctSummaryDTOList;
    }

    private AccountSummary copyAccountSummaryDAOToAccountSummaryDTO(com.poc.demo.client.eureka.domain.AccountSummary accountSummaryDAO){

        AccountSummary acctSummaryDTO = new AccountSummary();
            acctSummaryDTO.setId(accountSummaryDAO.getId());
            acctSummaryDTO.setName(accountSummaryDAO.getName());
            acctSummaryDTO.setAccountNumber(accountSummaryDAO.getAccountNumber());
            acctSummaryDTO.setAccountType(accountSummaryDAO.getAccountType());

        return acctSummaryDTO;
    }
}
