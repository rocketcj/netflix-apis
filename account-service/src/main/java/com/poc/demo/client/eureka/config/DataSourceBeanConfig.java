/**
 *
 */
package com.poc.demo.client.eureka.config;

import com.poc.demo.client.eureka.annotation.Dev;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

/**
 * Environment Specific Data Source Configurations.
 * <p>
 * As of now, there are 3 different environments defined, Dev, Qa and Prod.
 * The appropriate data source will be injected based on the active profile
 * defined in the <code>application.yml</code> file.
 * <p>
 * {@link Primary} annotation is used for all the data sources to ensure that these
 * beans get priority over any other bean defined a module specific packages.
 *
 * @author Chandrasekar Jayaraj
 */
@Configuration
public class DataSourceBeanConfig {

    private static final Logger LOG = LoggerFactory.getLogger(DataSourceBeanConfig.class);

    private static final String H2_DATABASE_NAME = "h2Database";

    private static final String[] ACCOUNT_SUMMARY_SCRIPTS = new String[]{
        "classpath:db/dev/pnc_accounts_create_schema.sql",
        "classpath:db/dev/pnc_accounts_insert.sql"
    };

    @Dev
    @Bean
    @Primary
    public DataSource devDataSource() {
        LOG.warn("==== Configuring DEV data source. Is this intentional? " +
            "If you in dev, this is good news. Else check you current active profile");
        return new EmbeddedDatabaseBuilder()
            .setType(EmbeddedDatabaseType.H2)
            .setName(H2_DATABASE_NAME)
            .addScripts(ACCOUNT_SUMMARY_SCRIPTS)
            .build();
    }

}
