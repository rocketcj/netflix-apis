package com.poc.demo.client.eureka.dao;

import com.poc.demo.client.eureka.domain.AccountSummary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by pl26565 on 5/24/17.
 */

@Repository
public interface AccountSummaryRepository extends JpaRepository<AccountSummary, Long>{

    List<AccountSummary> findAll();
    AccountSummary findByAccountNumber(Long accountNumber);

}
