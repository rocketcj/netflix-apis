package com.poc.demo.client.eureka.config;

import com.poc.demo.client.eureka.dao.AccountSummaryRepository;
import com.poc.demo.client.eureka.domain.AccountSummary;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Created by pl26565 on 5/25/17.
 */
@Configuration
@EnableJpaRepositories( basePackageClasses = AccountSummaryRepository.class )
@EntityScan (basePackageClasses = AccountSummary.class)
public class AccountBeansConfiguration {

    private final AccountSummaryRepository accountSummaryRepository;

    public AccountBeansConfiguration(AccountSummaryRepository accountSummaryRepository){
        this.accountSummaryRepository = accountSummaryRepository;

    }

   /* @Bean
    public AccountService accountService(){
         return new AccountServiceImpl(accountSummaryRepository);
    }*/


}
